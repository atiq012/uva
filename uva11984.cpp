#include<stdio.h>
int main()
{
    int t;
    float c,result,f;
    int i,n;
    scanf("%d",&n);
    for(i=0;i<n;i++)
    {
        scanf("%f %f",&c,&f);
        if(f==0)
        {
            result=c;
            printf("Case %d: %.02lf\n",i+1,result);
        }
        else if(f>0)
        {
            result=((9*c)/5)+32; // convert to F

            result = result+f;

            result = ((result-32)*5)/9;
            printf("Case %d: %.02lf\n",i+1,result);
        }
    }
    return 0;
}
